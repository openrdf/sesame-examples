This repository contains example code for working with OpenRDF Sesame. It is
primarily intended for tutorial purposes.


This work is licensed under the Creative Commons Attribution-ShareAlike 4.0
International License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/deed.en_US.
